# 19S-ITT2-project

weekly plans, resources and other relevant stuff for the project in IT technology 2. semester.

Interne links:
* [seneste lectureplan](https://gitlab.com/EAL-ITT/19s-itt2-project/-/jobs/artifacts/master/file/docs/19S_ITT2_lecture_plan.pdf?job=build+pdfs)
* [seneste project plan](https://gitlab.com/EAL-ITT/19s-itt2-project/-/jobs/artifacts/master/file/docs/project_plan_for_students.pdf?job=build+pdfs)
* [seneste gruppe liste](https://gitlab.com/EAL-ITT/19s-itt2-project/-/jobs/artifacts/master/file/docs/grouplist.pdf?job=build+pdfs)

public website: 
* [gitlab pages](https://eal-itt.gitlab.io/19s-itt2-project)
