---
title: 'ITT2 Project'
subtitle: 'Exercises'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>', 'Morten Bo Nielsen \<mon@ucl.dk\>']
main_author: 'Morten Nielsen'
date: \today
email: 'mon@ucl.dk'
left-header: \today
right-header: 'ITT2 project, exercises'
---


Introduction
====================

This document is a collection of exercises. They are associated with the weekly plans. 

References

* [Generic Project plan](https://eal-itt.gitlab.io/19s-itt2-project/project_plan_for_students.pdf)
* [Weekly plans](https://eal-itt.gitlab.io/19s-itt2-project/19S_ITT2_weekly_plans.pdf)



