---
Week: 17
Content:  Project part 2 - phase 1
Material: See links in weekly plan
Initials: MON/NISI
---

# Week 17 Project part 2 startup

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals


### Learning goals
* Basic gitlab project management
  * Level 1: Know what gitlab.com is in a project management context.
  * Level 2: Able to set up and use issues in gitlab.com
  * Level 3: Able to use gitlab.com for project management

* Basic project definitions and description
  * Level 1: Know the important elements when defining projects
  * Level 2: Able to define a project.
  * Level 3: Able to define a project and use it as part of running the project


## Deliverable
* Mandatory weeky meetings with the teachers (this includes minutes of the meeting)
  * Agenda is:
    1. Status on project (ie. show closed tasks in gitlab)
    2. Next steps (ie. show next tasks in gitlab)
    3. Collaboration within the group (ie. any internal issues, fairness of workload, communication)
    4. Help needed or offered (ie. what help do you need and where do you feel you can contribute to the class)
* 
* Complete project plan - include goals and deliverables. The teachers have supplied a project plan with missing parts.
* All groups have performed a pre-mortem meeting and included minutes of meeting in project plan
* All groups have created a new gitlab project.
* Project phases has been created as milestones in gitlab (deliverable issues HAS to be assigned to a milestone)

## Schedule

Tuesday

* 8:15 Introduction to the project by NISI and MON

    We will go through the expectations and discuss the project and the plans.
    
    Rundown of project part 2 phases, projectplan, deliverables and Gitlab.

* 9:00 Exercise 0: Pre-mortem meeting

* 11:00 Forming groups

    Form groups of three... no-one in the same group as previous groups

    Create project on gitlab, and include teachers

* 13:00 Presentations in network lab

    Each group presents their project plan and gitlab project to MON and NISI. Everyone else is welcome to attend the presentations.


Tuesday

* 8:15 Introdution to the day, general Q/A session

* 8:30 Exercise 2: Define tasks

    This is a 30 minute timeslot. Notice that. It means that you are not to do something too elaborated.

* 9:00 Group morning meeting

    You will meet in your groups every morning and decide on who does what. This is part of the project management, and is mandatory.
    Ordinary agenda:
    1. (5 min) Round the table: What did I do, and what did I finish?
    2. (5-10 min) Review of tasks: Are they still relevant? do we need to add new ones?
    3. (5 min) Round the table: Claim one task each.

    Today only 3. is relevant.

* 9:15 Teacher meetings

    Timeslot for you weekly meeting with the teachers.
    
    Remember to book a time and have an agenda prepared.

* 10:00 Study trip presentation 
    
    PDA and others will present the upcoming trip to Fontys in Holland. 

* 11:00 Teacher meetings

    Timeslot for you weekly meeting with the teachers.
    
* 12:30 Work on deliverables 


## Hands-on time

* Exercise 0: Pre-mortem meeting

    We will do a pre-mortem meeting. (in 3 groups)

    1. (20 min) Imagine that we fail in implementing the project. In the group, come up with as many reasons as possible for that to have happened.

    2. Select the top 5 most relevant.
        relevant might be equivalent to most likely in this context
        exclude stuff we don't have any control over, e.g. meteor showers.

    3. Write them on the whiteboard

    4. (30 min) On class, we will discuss how to prevent the problems from arizing.
        this includes who does what.
    
    
* Exercise 1: Project plan

    The teachers have provided a project plan. Look it over, and notice that there are things missing.
    
    Fill out the missing sections and include the risk assessment from the pre-mortem meeting.
    

* Exercise 2: Define tasks

    Look throught the project plan, and define tasks that ensures yo ureach the goals.
    
    Remember:
    * tasks related to group management, ie. coordiantion, minutes of meeting, discussions and decision on how to collaborate
    * tasks related to documentation
    * tasks realted to research, information gathering and sharing information on gitlab
    * tasks related to building and testing
    * good tasks are SMART and take less than 1 hour


## Comments
* Please look into the material from December if your git skills are rusty
* Read about SMART tasks [here](https://gigaom.com/2008/05/05/how-to-create-smart-tasks/)
