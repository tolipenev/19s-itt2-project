---
Week: 11
Content: Project part 1 phase 3
Material: See links in weekly plan
Initials: MON/NISI
---

# Week 11 Make it useful for the user

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* All groups have implemented the status led extension
* All groups have updated the API client and server
* All groups have a sketch showing minimum system housing

### Learning goals
* System extension
    * level 1: The student is able to extend the minimum system with a blinking led when messages is received from uart 
    * level 2: The student is able to extend the minimum system with a blinking led showing multiple status messages
    * level 3: The student is able to extend the minimum system with a pwm controlled led showing multiple status messages 

* Housing sketch
    * level 1: The student is able to generate an idea of housing
    * level 2: The student is able to generate an idea of housing and explain why it is useful for the user
    * level 3: The student is able to generate an idea of housing and verify it by user testing 

## Deliverables
* Mandatory weeky meetings with the teachers (this includes minutes of the meeting)
  * Agenda is:
    1. Status on project (ie. show closed tasks in gitlab)
    2. Next steps (ie. show next tasks in gitlab)
    3. Collaboration within the group (ie. any internal issues, fairness of workload, communication)
    4. Evaluation of test result from ww10 (show checklists) 
    5. Help needed or offered (ie. what help do you need and where do you feel you can contribute to the class)
    6. Any other business (AOB)

* Status LED extension
* Raspberry API client and Server API update to be able to handle Status LED extension
* Sketch showing housing for the minimum system hardware
* A rudimentary user manual for the system you will have in 3 weeks.


## Schedule

Monday

* 8:15 Introdution to the day, general Q/A session

  NISI will do a quick introduction to the exercises including PWM and UART 

* 8:45 Group morning meeting

    You will meet in your groups every morning and decide on who does what. This is part of the project management, and is mandatory.
    Ordinary agenda:
    1. (5 min) Round the table: What did I do, and what did I finish?
    2. (5-10 min) Review of tasks: Are they still relevant? do we need to add new ones?
    3. (5 min) Round the table: Claim one task each.

* 9:00 You work on deliverables

    Remember to come ask questions if you have any.  

Tuesday

* 8:15 Introdution to the day, general Q/A session

    We will also make a list of who goes to fablab on which day.

* 8:30 Group morning meeting (See monday for agenda)

* 9:00 Teacher meetings

    Timeslot for you weekly 10 min. meeting with the teachers.

    Remember to book a time and have an agenda prepared.

* 9:00 You work on deliverables.

    Come ask us if you have questions.

## Hands-on time

See the [exercise document](https://eal-itt.gitlab.io/19s-itt2-project/19S_ITT2_exercises.pdf) for details.

## Comments

* thingiverse has some cool [raspberry pi projects](https://www.thingiverse.com/search?q=raspberry+pi).
* Some links to OpenScad
    * [Part 1 on youtube (in Danish)](https://www.youtube.com/watch?v=INyVtZfIVvE)
    * [Part 2 blog entry](https://moozing.wordpress.com/2019/03/02/lasercutting-part-2/)