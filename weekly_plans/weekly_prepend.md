---
title: 'ITT2 Project'
subtitle: 'Weekly plans'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>', 'Morten Bo Nielsen \<mon@ucl.dk\>']
main_author: 'Morten Nielsen'
date: \today
email: 'mon@ucl.dk'
left-header: \today
right-header: 'ITT2 project, weekly plans'
---


Introduction
====================

This document is a collection of weekly plans. It is based on the wekly plans in the administrative repository, and is updated automatically on change.

The sections describe the goals and programm for each week of the second semester project.

For the project details, we refer to the [generic project plan](https://eal-itt.gitlab.io/19s-itt2-project/project_plan_for_students.pdf)
